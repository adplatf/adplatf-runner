package ru.serega6531.adplatf;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.serega6531.adplatf.amqp.CheckAction;
import ru.serega6531.adplatf.amqp.CheckerRequest;
import ru.serega6531.adplatf.amqp.CheckerResult;
import ru.serega6531.adplatf.configuration.PlatformConfiguration;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
@SuppressWarnings({"unused", "RedundantSuppression"})
public class CheckerListener {

    private final PlatformConfiguration configuration;

    private final String redisHost;
    private final String redisPassword;

    @Autowired
    public CheckerListener(PlatformConfiguration configuration,
                           ConfigurationUtils configurationUtils,
                           @Value("${REDIS_HOST}") String redisHost,
                           @Value("${REDIS_PASSWORD}") String redisPassword) {
        this.configuration = configuration;
        this.redisHost = redisHost;
        this.redisPassword = redisPassword;
    }

    @RabbitListener(queues = "checker_input")
    public CheckerResult run(CheckerRequest request) {
        PlatformConfiguration.Service service = configuration.getServices().get(request.getService());
        PlatformConfiguration.Team team = configuration.getTeams().get(request.getTeam());
        CheckAction action = request.getAction();
        String flag = request.getFlag();

        ExecutionResult result = switch (action) {
            case CHECK -> doCheck(team, service);
            case PULL -> doPull(team, service, flag);
            case PUSH -> doPush(team, service, flag);
        };

        return new CheckerResult(request.getTeam(), request.getService(), request.getAction(), request.isExpectedPush(),
                result.getOut(), result.getErr(), result.isError(), result.getExecutionTime(), request.getRequestTime());
    }

    private ExecutionResult doCheck(PlatformConfiguration.Team team, PlatformConfiguration.Service service) {
        log.info("Running checks on team {}, service {}", team.getName(), service.getName());

        String script = service.getCheckFunctionalityScript();
        String ip = team.getIp();

        return executeCommand(String.join(" ", script, redisHost, redisPassword, ip));
    }

    private ExecutionResult doPull(PlatformConfiguration.Team team, PlatformConfiguration.Service service, String oldFlag) {
        log.info("Pulling flag {} from team {}, service {}", oldFlag, team.getName(), service.getName());

        String script = service.getPullFlagScript();
        String ip = team.getIp();

        return executeCommand(String.join(" ", script, redisHost, redisPassword, ip, oldFlag));
    }

    private ExecutionResult doPush(PlatformConfiguration.Team team, PlatformConfiguration.Service service, String newFlag) {
        log.info("Pushing flag {} to team {}, service {}", newFlag, team.getName(), service.getName());

        String script = service.getPushFlagScript();
        String ip = team.getIp();

        return executeCommand(String.join(" ", script, redisHost, redisPassword, ip, newFlag));
    }

    private ExecutionResult executeCommand(String command) {
        log.trace("Executing script {}", command);
        long startTime = System.currentTimeMillis();

        try {
            Process proc = new ProcessBuilder("bash", "-c", command).start();
            boolean finished = proc.waitFor(configuration.getCheckInterval(), TimeUnit.SECONDS);

            int executionTime = (int) (System.currentTimeMillis() - startTime);
            String out = readFromStream(proc.getInputStream());
            String err = readFromStream(proc.getErrorStream());

            if (!finished) {
                log.warn("Timeout while executing script {}", command);
                proc.destroy();
                String errorMessage = String.format("Checker timed out. Output so far: %s, err: %s", out, err);
                return new ExecutionResult(true, "Checker timeout", errorMessage, executionTime);
            }

            int exitCode = proc.exitValue();
            return new ExecutionResult(exitCode != 0, out, err, executionTime);
        } catch (IOException | InterruptedException e) {
            log.error("Error executing script \"" + command + "\"", e);
            Thread.currentThread().interrupt();
            int executionTime = (int) (System.currentTimeMillis() - startTime);
            return new ExecutionResult(true, "Checker error", "ERROR: " + e.getMessage(), executionTime);
        }
    }

    @SneakyThrows
    private String readFromStream(InputStream stream) {
        try (final StringWriter writer = new StringWriter();
             final InputStreamReader reader = new InputStreamReader(stream)) {
            reader.transferTo(writer);
            return writer.toString();
        }
    }

    @Data
    @AllArgsConstructor
    private static class ExecutionResult {
        private boolean isError;
        private String out;
        private String err;
        private int executionTime;
    }

}
