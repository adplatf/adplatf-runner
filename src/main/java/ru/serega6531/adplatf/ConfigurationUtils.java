package ru.serega6531.adplatf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.serega6531.adplatf.configuration.PlatformConfiguration;

import java.util.Optional;

@Service
public class ConfigurationUtils {

    private final PlatformConfiguration configuration;

    @Autowired
    public ConfigurationUtils(PlatformConfiguration configuration) {
        this.configuration = configuration;
    }

    public Optional<PlatformConfiguration.Service> findService(String name) {
        return configuration.getServices().stream()
                .filter(t -> t.getName().equals(name))
                .findFirst();
    }

    public Optional<PlatformConfiguration.Team> findTeam(String name) {
        return configuration.getTeams().stream()
                .filter(t -> t.getName().equals(name))
                .findFirst();
    }

}
