package ru.serega6531.adplatf.amqp;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class CheckerResult implements Serializable {

    private static final long serialVersionUID = 7414794024583296196L;

    private int team;
    private int service;
    private CheckAction action;
    private boolean expectedPush;
    private String executionOut;
    private String executionErr;
    private boolean isError;
    private int executionTime;
    private LocalDateTime requestTime;

}
