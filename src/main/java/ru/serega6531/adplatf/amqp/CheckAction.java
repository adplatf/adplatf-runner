package ru.serega6531.adplatf.amqp;

import lombok.Getter;

public enum CheckAction {

    CHECK("check"), PULL("pull"), PUSH("push");

    @Getter
    private final String name;

    CheckAction(String name) {
        this.name = name;
    }

}
