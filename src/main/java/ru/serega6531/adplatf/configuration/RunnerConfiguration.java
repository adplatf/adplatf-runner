package ru.serega6531.adplatf.configuration;

import lombok.Getter;
import lombok.NoArgsConstructor;

@SuppressWarnings({"unused", "RedundantSuppression"})
@Getter
@NoArgsConstructor
public class RunnerConfiguration {

    private int concurrentConsumers;
    private int maxConcurrentConsumers;

}
