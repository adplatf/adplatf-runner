package ru.serega6531.adplatf.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Declarable;
import org.springframework.amqp.core.Declarables;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public PlatformConfiguration platformConfiguration(ObjectMapper objectMapper) throws IOException {
        return readConfiguration("config.json", PlatformConfiguration.class, objectMapper);
    }

    @Bean
    public RunnerConfiguration runnerConfiguration(ObjectMapper objectMapper) throws IOException {
        return readConfiguration("runner.json", RunnerConfiguration.class, objectMapper);
    }

    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(ConnectionFactory connectionFactory,
                                                                               RunnerConfiguration runnerConfiguration) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();

        factory.setConnectionFactory(connectionFactory);
        factory.setPrefetchCount(1);
        factory.setConcurrentConsumers(runnerConfiguration.getConcurrentConsumers());
        factory.setMaxConcurrentConsumers(runnerConfiguration.getMaxConcurrentConsumers());
        factory.setAcknowledgeMode(AcknowledgeMode.AUTO);
        factory.setTaskExecutor(Executors.newFixedThreadPool(runnerConfiguration.getMaxConcurrentConsumers()));

        return factory;
    }

    @Bean
    public Declarables qs(PlatformConfiguration configuration) {
        List<Declarable> queues = new ArrayList<>();
        String[] queueNames = new String[] {"checker_input"};

        for (String name : queueNames) {
            queues.add(new Queue(name,
                    true, false, false,
                    Map.of("x-message-ttl", TimeUnit.SECONDS.toMillis(configuration.getQueueTtl()))));
        }

        return new Declarables(queues);
    }

    private <T> T readConfiguration(String filename, Class<T> type, ObjectMapper objectMapper) throws IOException {
        File configFile = new File(filename);

        if (!configFile.exists()) {
            throw new IllegalStateException(filename + " does not exist in " + new File(".").getAbsolutePath());
        }

        return objectMapper.readValue(configFile, type);
    }

}
