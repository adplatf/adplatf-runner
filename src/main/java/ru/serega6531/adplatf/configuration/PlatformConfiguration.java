package ru.serega6531.adplatf.configuration;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@SuppressWarnings({"unused", "RedundantSuppression"})
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlatformConfiguration {

    private String ctfTitle;

    private int checkInterval;   // in seconds
    private int checksInRound;   // so checkInterval * (checksInRound + 1) = round length
    // (cause there's also an interval between the last check and the next round start)
    private int totalRounds;
    private int flagLifeLength;  // rounds before the flag expire
    private int queueTtl;        // for how long can a check message wait for processing
    @JsonIgnore
    private LocalDateTime gameStartTime;
    @JsonIgnore
    private LocalDateTime freezeStartTime;  // could be null for no freeze time

    private int scoreHardness;
    private boolean scoreInflation;

    private List<Team> teams;
    private List<Service> services;

    @Getter
    @NoArgsConstructor
    public static class Team {
        private String name;
        private String ip;
        private String token;
        private String icon;
    }

    @Getter
    @NoArgsConstructor
    public static class Service {
        private String name;
        private double initialScore;
        private double firstBloodBonus;  // could be zero

        // Checkers execution scripts, i.e. "/checker/check.sh" or "python checker.py --custom-param"
        private String checkFunctionalityScript;  // redis host, password and ip are appended to arguments
        private String pushFlagScript;            // redis host, password, ip and old flag are appended to arguments
        private String pullFlagScript;            // redis host, password, ip and new flag are appended to arguments
    }

}
