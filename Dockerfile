FROM adoptopenjdk/openjdk14:alpine
WORKDIR /tmp/runner

COPY src ./src
COPY gradle ./gradle
COPY *.gradle ./
COPY gradlew ./
RUN ./gradlew --no-daemon build

FROM adoptopenjdk/openjdk14:debianslim-jre

COPY --from=0 /tmp/runner/build/libs/adplatf-worker-*.jar /app.jar
COPY config.json /
COPY runner.json /
COPY checkers /checkers
CMD ["java", "-jar", "/app.jar"]